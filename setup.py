from setuptools import setup, Command

class my_clean(Command):

    description = "Removes the generated files from the directory"
    user_options = []

    def initialize_options(self):
        pass

    def run(self):
        import os, shutil

        try:
            os.remove('MANIFEST')
        except:
            pass

        dirs = ['vfbi/__pycache__', 'vfbi.egg-info', 'build', 'dist']

        for dir in dirs:
            shutil.rmtree(dir, True)

    def finalize_options(self):
        pass

with open('README.rst') as readme:
    long_description = readme.read()

setup(
    name='vfbi',
    version='0.2.0',
    description='Software to interface with the VFBI interrogator',
    author='Lucas Hermann Negri',
    author_email='lucashnegri@gmail.com',
    url='https://bitbucket.org/lucashnegri/vfbi',
    packages=['vfbi'],
    install_requires=['pyqtgraph', 'pyserial'],
    cmdclass={
        'clean': my_clean,
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering'
    ],
    license='MIT',
    scripts = ['vfbi/vfbi']
)
