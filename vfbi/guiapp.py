from PyQt4 import QtGui
from vfbi.mainwindow import MainWindow
import sys

class GuiApp:
    def __init__(self, device, port=None, repeat=None, out=None):
        self.app = QtGui.QApplication(sys.argv)
        self.window = MainWindow(device, port, out)

    def run(self):
        def handle_error(type_, value, traceback):
            self.window.stopAcquire()
            QtGui.QMessageBox.warning(self.window, "Exception", str(value))
        
        # install hook
        sys.excepthook = handle_error
        
        self.window.show()
        code = self.app.exec_()
        self.app.deleteLater()
        sys.exit(code)
        
