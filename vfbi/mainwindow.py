from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt
from vfbi.ui.mainwindow import Ui_MainWindow
from datetime import datetime
from array import array
from serial.tools import list_ports
import vfbi
import sys

class Worker(QtCore.QObject):
    finished = QtCore.pyqtSignal()
    dataAcquired = QtCore.pyqtSignal(array)
    
    def __init__(self, device):
        super().__init__()
        self.device = device
    
    @QtCore.pyqtSlot()
    def acquire(self):
        data = self.device.acquire()
        self.dataAcquired.emit(data)
        self.finished.emit()

class MainWindow(QtGui.QMainWindow, Ui_MainWindow):
    
    def __init__(self, device, port=None, out=None):
        # GUI
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)

        self.message = QtGui.QLabel(textInteractionFlags=
            Qt.TextSelectableByKeyboard | Qt.TextSelectableByMouse)
        self.statusbar.addWidget(self.message, 1.)
        font = self.message.font()
        font.setBold(True)
        self.message.setFont(font)
        self.setMessage("VFBI version {}".format(vfbi.__version__))

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.acquire)

        self.device = device
        self.updateStatus()
        
        self.refresh()
        
        if port:
            self.comboPort.insertItem(0, port)
            self.comboPort.setCurrentIndex(0)
        
        if out:
            self.lineData.setText(out)
        
        # Worker thread (IO)
        self.io_thread = QtCore.QThread()
        self.worker = Worker(self.device)
        self.worker.moveToThread(self.io_thread)
        self.io_thread.started.connect(self.worker.acquire)
        self.worker.finished.connect(self.io_thread.quit)
        self.worker.dataAcquired.connect(self.dataAcquired)

    @QtCore.pyqtSlot()
    def acquire(self):
        self.io_thread.start()
    
    @QtCore.pyqtSlot()
    def refresh(self):
        self.comboPort.clear()
        
        for port, desc, id_ in list_ports.comports():
            self.comboPort.addItem(port)
    
    @QtCore.pyqtSlot(array)
    def dataAcquired(self, data):
        self.plt.plot(data)
        self.inform("Signal acquired at " + str(datetime.now()))
        vfbi.log(self.getDataPath(), data)
        
    @QtCore.pyqtSlot()
    def toggleConnection(self):
        try:
            if self.device.connected:
                self.stopAcquire()
                self.device.disconnect()
            else:
                port = self.comboPort.currentText()
                self.device.connect(port)
        finally:
            self.updateStatus()

    @QtCore.pyqtSlot(bool)
    def toggleAcquire(self, on):
        if on:
            self.timer.setInterval(self.getDelay())
            self.timer.start()
            self.acquire()
        else:
            self.timer.stop()
            self.io_thread.quit()

    @QtCore.pyqtSlot()
    def selectData(self):
        path = QtGui.QFileDialog.getSaveFileName(self, "Select data file",
                           options=QtGui.QFileDialog.DontConfirmOverwrite)
        self.lineData.setText(path)
    
    def inform(self, msg):
        self.statusbar.showMessage(msg, 2000)
    
    def stopAcquire(self):
        try:
            self.pushAcquire.setChecked(False)
            self.io_thread.wait(2000)
        except Exception as e:
            print(str(e), file=sysi)

    def updateStatus(self):
        up = self.device.connected

        if up:
            msg = "Connected to {}.".format(self.device.name)
        else:
            msg = "Disconnected."

        self.setMessage(msg)
        self.pushConnect.setChecked(up)
        self.comboPort.setEnabled(not up)
        self.pushRefresh.setEnabled(not up)
        self.pushAcquire.setEnabled(up)

    def setMessage(self, msg):
        self.message.setText(msg)

    def getDataPath(self):
        return self.lineData.text().strip()

    def getDelay(self):
        return self.spinDelay.value()*1000

    def closeEvent(self, evnt):
        self.stopAcquire()
