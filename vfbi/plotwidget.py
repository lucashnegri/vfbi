from pyqtgraph import PlotWidget

class PlotWidget(PlotWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.item = self.getPlotItem()
        self.item.showGrid(x=True, y=True)
        self.item.setLabels(left="Power", bottom="Sample")
        self.item.setRange(xRange=(0.0, 1.0), yRange=(0.0, 1.0))

    def plot(self, data):
        try:
            self.line.setData(data)
        except:
            self.line = self.item.plot(data)
            self.autoRange()