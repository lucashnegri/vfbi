# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created: Wed Oct 15 10:18:15 2014
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(729, 528)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setUsesScrollButtons(False)
        self.tabWidget.setDocumentMode(True)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.verticalLayout = QtGui.QVBoxLayout(self.tab)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label = QtGui.QLabel(self.tab)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label)
        self.comboPort = QtGui.QComboBox(self.tab)
        self.comboPort.setEditable(True)
        self.comboPort.setInsertPolicy(QtGui.QComboBox.NoInsert)
        self.comboPort.setObjectName(_fromUtf8("comboPort"))
        self.horizontalLayout_2.addWidget(self.comboPort)
        self.pushRefresh = QtGui.QPushButton(self.tab)
        self.pushRefresh.setObjectName(_fromUtf8("pushRefresh"))
        self.horizontalLayout_2.addWidget(self.pushRefresh)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pushConnect = QtGui.QPushButton(self.tab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushConnect.sizePolicy().hasHeightForWidth())
        self.pushConnect.setSizePolicy(sizePolicy)
        self.pushConnect.setCheckable(True)
        self.pushConnect.setFlat(False)
        self.pushConnect.setObjectName(_fromUtf8("pushConnect"))
        self.horizontalLayout.addWidget(self.pushConnect)
        self.horizontalLayout_2.addLayout(self.horizontalLayout)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushAcquire = QtGui.QPushButton(self.tab)
        self.pushAcquire.setCheckable(True)
        self.pushAcquire.setObjectName(_fromUtf8("pushAcquire"))
        self.horizontalLayout_2.addWidget(self.pushAcquire)
        self.horizontalLayout_2.setStretch(4, 1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.plt = PlotWidget(self.tab)
        self.plt.setObjectName(_fromUtf8("plt"))
        self.verticalLayout.addWidget(self.plt)
        self.verticalLayout.setStretch(1, 1)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.tab_2)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_3 = QtGui.QLabel(self.tab_2)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_3)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.lineData = QtGui.QLineEdit(self.tab_2)
        self.lineData.setMinimumSize(QtCore.QSize(300, 0))
        self.lineData.setObjectName(_fromUtf8("lineData"))
        self.horizontalLayout_3.addWidget(self.lineData)
        self.pushSelectData = QtGui.QPushButton(self.tab_2)
        self.pushSelectData.setObjectName(_fromUtf8("pushSelectData"))
        self.horizontalLayout_3.addWidget(self.pushSelectData)
        self.horizontalLayout_3.setStretch(0, 1)
        self.formLayout.setLayout(0, QtGui.QFormLayout.FieldRole, self.horizontalLayout_3)
        self.label_2 = QtGui.QLabel(self.tab_2)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_2)
        self.spinDelay = QtGui.QDoubleSpinBox(self.tab_2)
        self.spinDelay.setDecimals(3)
        self.spinDelay.setMaximum(9999.0)
        self.spinDelay.setSingleStep(0.1)
        self.spinDelay.setProperty("value", 1.0)
        self.spinDelay.setObjectName(_fromUtf8("spinDelay"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.spinDelay)
        self.horizontalLayout_4.addLayout(self.formLayout)
        spacerItem1 = QtGui.QSpacerItem(304, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem1)
        self.horizontalLayout_4.setStretch(1, 1)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.verticalLayout_2.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.label.setBuddy(self.comboPort)
        self.label_3.setBuddy(self.lineData)
        self.label_2.setBuddy(self.spinDelay)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.pushConnect, QtCore.SIGNAL(_fromUtf8("clicked()")), MainWindow.toggleConnection)
        QtCore.QObject.connect(self.pushAcquire, QtCore.SIGNAL(_fromUtf8("toggled(bool)")), MainWindow.toggleAcquire)
        QtCore.QObject.connect(self.pushSelectData, QtCore.SIGNAL(_fromUtf8("clicked()")), MainWindow.selectData)
        QtCore.QObject.connect(self.pushRefresh, QtCore.SIGNAL(_fromUtf8("clicked()")), MainWindow.refresh)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.comboPort, self.pushRefresh)
        MainWindow.setTabOrder(self.pushRefresh, self.pushConnect)
        MainWindow.setTabOrder(self.pushConnect, self.pushAcquire)
        MainWindow.setTabOrder(self.pushAcquire, self.tabWidget)
        MainWindow.setTabOrder(self.tabWidget, self.lineData)
        MainWindow.setTabOrder(self.lineData, self.pushSelectData)
        MainWindow.setTabOrder(self.pushSelectData, self.spinDelay)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "VFBI", None))
        self.label.setText(_translate("MainWindow", "&Device:", None))
        self.pushRefresh.setText(_translate("MainWindow", "&Refresh", None))
        self.pushConnect.setText(_translate("MainWindow", "&Connect", None))
        self.pushAcquire.setText(_translate("MainWindow", "&Acquire", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "&Acquisition", None))
        self.label_3.setText(_translate("MainWindow", "&Data file", None))
        self.pushSelectData.setText(_translate("MainWindow", "...", None))
        self.label_2.setText(_translate("MainWindow", "&Delay", None))
        self.spinDelay.setSuffix(_translate("MainWindow", " s", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "&Options", None))

from vfbi.plotwidget import PlotWidget
