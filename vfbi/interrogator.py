import serial
from array import array

class Interrogator:
    '''Handles the connection to the VFBI interrogator.'''

    baudrate = 115200
    bytesize = serial.EIGHTBITS
    parity = serial.PARITY_NONE
    stopbits = serial.STOPBITS_ONE
    resolution = 2**12
    ask_byte = bytes([1])

    def __init__(self):
        self.conn = serial.Serial(baudrate=self.baudrate,
                                  bytesize=self.bytesize,
                                  parity=self.parity,
                                  stopbits=self.stopbits)

    def __str__(self):
        if self.conn.isOpen():
            status = "connected to " + self.name
        else:
            status = "disconnected"

        return "Interrogator(), {}".format(status)

    def connect(self, port, timeout=1.0):
        '''Connects to a VFBI device

        Parameters
        ----------
        port : string
            Device name
        timeout : float
            Timeout for the write/read operations, in seconds
        '''
        self.disconnect()
        self.conn.port = port
        self.conn.timeout = timeout
        self.conn.writeTimeout = timeout
        self.conn.open()

    def disconnect(self):
        '''Disconnects from the device'''
        self.conn.close()

    def acquire(self):
        '''Performs one signal acquisition

        Returns
        -------
        list :
            Acquired signal, as a list of ints
        '''
        self.conn.write(self.ask_byte) # ask
        data = self.conn.read(self.resolution) # get data
        return array('i', data)

    @property
    def connected(self):
        return self.conn.isOpen()

    @property
    def name(self):
        return self.conn.name
