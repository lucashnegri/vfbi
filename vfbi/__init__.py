__version__ = '0.2.0'
__author__ = 'Lucas Hermann Negri <lucashnegri@gmail.com>'

def log(path, data):
    if path:
        with open(path, "a") as file:
            line = ' '.join(map(str, data))
            file.write(line)
            file.write('\n')
