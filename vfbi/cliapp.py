import vfbi

class CliApp:
    def __init__(self, device, port, repeat, out):
        self.device = device
        self.port   = port
        self.repeat = repeat
        self.out    = out
        
    def run(self):
        self.device.connect(self.port)
        
        for r in range(self.repeat):
            data = self.device.acquire()
            vfbi.log(self.out, data)
        
        self.device.disconnect()
        
