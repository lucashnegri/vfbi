VFBI
====

VFBI handles the data acquisition from the VFBI FBG/LPG interrogador.

Installation
------------

To install VFBI from the source package, run:

.. code-block:: bash
    
    python setup.py install
    
VFBI targets Python 3.4 and depends on PyQt4, PyQtGraph and pySerial.

Contribute
----------

- Source Code: https://bitbucket.org/lucashnegri/vfbi
- Issues: https://bitbucket.org/lucashnegri/vfbi/issues
- Direct contact: Lucas Hermann Negri - lucashnegri <at> gmail.com

License
-------

The project is licensed under the MIT license.
