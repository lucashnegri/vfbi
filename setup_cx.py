import sys
from cx_Freeze import setup, Executable

build_exe_options = {
    'packages': ['pyqtgraph',  'scipy', 'serial'],
    'include_files': [
        (r'C:\dev\Anaconda3\Lib\site-packages\numpy\core\libifcoremd.dll',  'libifcoremd.dll'),
        (r'C:\dev\Anaconda3\Lib\site-packages\numpy\core\libmmd.dll',  'libmmd.dll'),
    ],
    'excludes': ['tkinker', 'tk', 'tcl'] 
}

setup( 
    name = 'vfbi',
    version = '0.2.0',
    description = 'VFBI interface',
    author = 'LHN',
    options = {'build_exe': build_exe_options},
    executables = [
        Executable(
            'vfbi/vfbi',
            base='Win32GUI',
            shortcutName="VFBI",
            shortcutDir="ProgramMenuFolder",
        )
    ]
)
